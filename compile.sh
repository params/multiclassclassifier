#! /bin/bash

SOURCES=" ./src/org/HBLR/base/PIF.java \
    ./src/org/HBLR/base/PIFArray.java \
    ./src/org/HBLR/base/Example.java \
    ./src/org/HBLR/base/WeightParameter.java \
    ./src/org/pMLR/ML/Mcsrch.java \
    ./src/org/pMLR/ML/LBFGS.java \
    ./src/org/pMLR/ML/LogisticRegression.java \
    ./src/org/pMLR/ML/BinarySVM.java \
    ./src/org/pMLR/hadoop/Converter.java \
    ./src/org/pMLR/hadoop/TrainingDriver.java \
    ./src/org/pMLR/hadoop/TestingDriver.java";

rm -rf ./bin
mkdir -p ./bin

#Creating the class files
for src in `echo $SOURCES`;
do
    javac -d ./bin -classpath .:./lib/*:./bin $src
    echo $src
done
echo "Compiled successfully.."

#Creating the jar
jar cvf MulticlassClassifier.jar -C bin .
jar uvf MulticlassClassifier.jar -C src .
jar uvf MulticlassClassifier.jar -C lib .

#Uncomment line below to inspect contents of the created jar
#jar tf MulticlassClassifier.jar
echo "Created jar successfully.."

